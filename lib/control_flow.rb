# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
	str.delete(str.downcase)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
	if str.length % 2 == 0
		return str[str.length/2-1..str.length/2]
	else
		return str[str.length/2]
	end
end

# Return the number of vowels in a string.
def num_vowels(str)
	vowels = "aeiouAEIOU"
	str.count(vowels)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
	(1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator ="")
    join = ""
    arr.each_index do |i|
    join << arr[i]
    join << separator unless i == arr.length - 1
end
join
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
	result = ""
	str.chars.each_index do |idx|
		if idx.odd?
			result << str[idx].upcase
		else
			result << str[idx].downcase
		end
	end
result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
	result = []
	str.split.each do |word|
		if word.length >= 5
			result << word.reverse
		else
			result << word
		end
	end
	result.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
	result = []
	(1..n).each do |num|
		if num % 15 == 0
			result << "fizzbuzz"
		elsif num % 3 == 0
			result << "fizz"
		elsif num % 5 == 0
			result << "buzz"
		else
			result << num
		end
	end
	result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
	result = []
	i = -1
	until result.length == arr.length
		result << arr[i]
		i -= 1
	end
	result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
	factors = (1..num).select { |factor| num % factor == 0 }
	if factors.length <= 2
		return true
	else
		return false
	end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
	(1..num).select { |factor| num % factor == 0 }
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime?(num)
  return false if num == 1
	factors = (1..num).select { |factor| num % factor == 0 }
	if factors.length <= 2
		return true
	else
		return false
	end
end

def prime_factors(num)
	(1..num).select { |factor| num % factor == 0 && prime?(factor) }
end

# OR...with both helper methods

def prime_factors(num)
	factors(num).select { |factor| prime?(factor) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
	prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
	odd_integers = arr.select { |num| num.odd? }
	even_integers = arr.select { |num| num.even? }
	if odd_integers.length == 1
		return odd_integers[0]
	else
		return even_integers[0]
	end
end
